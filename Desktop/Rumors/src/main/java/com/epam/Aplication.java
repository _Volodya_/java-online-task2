package com.epam;

import java.util.Scanner;

public class Aplication {
    public static void main(String[] args) {
        final int attempts = 40;
        Scanner scanner = new Scanner(System.in);
        System.out.println(" Enter how mach people should be at the party ");
        System.out.println(" Number should be >2 ");
        int n = scanner.nextInt();
        int countTimesFullSpreaded = 0;
        int peopleReached = 0;
        for (int i = 0; i < attempts; i++)
        {
            boolean guests[] = new boolean[n];

            guests[1] = true;
            boolean alredyHeard = false;
            int nextPerson = -1;
            int currentPerson = 1;
            while (!alredyHeard)
            {
                nextPerson = 1 + (int) (Math.random() * (n - 1));
                if (nextPerson == currentPerson) {
                    while (nextPerson == currentPerson)
                        nextPerson = 1 + (int) (Math.random() * (n - 1));
                }
                if (guests[nextPerson])
                {
                    if (rumorSpreaded(guests))
                        countTimesFullSpreaded++;
                    peopleReached = peopleReached + countPeopleReached(guests);
                    alredyHeard = true;

                }
                guests[nextPerson] = true;
                currentPerson = nextPerson;
            }
        }
        System.out.println(" Empirical probability that everyone will hear  rumor except Alice in " + attempts +
                " attempts: " + (double) countTimesFullSpreaded / attempts);
        System.out.println(" Average amount of people the rumor reached is " + peopleReached / attempts);
    }

    public static int countPeopleReached(boolean arr[]) {
        int counter = 0;
        for (int i = 1; i < arr.length; i++)
            if (arr[i])
                counter++;
        return counter;
    }

    public static boolean rumorSpreaded(boolean arr[]) {
        for (int i = 1; i < arr.length; i++)
            if (!arr[i])
                return false;
        return true;
    }
}